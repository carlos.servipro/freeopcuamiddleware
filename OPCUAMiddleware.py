import opcua
import time
from opcua import Server, ua, uamethod
from datetime import datetime
from ping3 import ping, verbose_ping
from enum import Enum



ipModulo="192.168.15.250"
url = "opc.tcp://"+ipModulo+":4840"
urlServer = "opc.tcp://192.168.15.60:4840"

#Client Config
client= opcua.Client(url)
client.connect()

@uamethod
def ISDUWrite(parent, index, subindex, value):
    try:
        cmd=b'\x00\x00\x00\x00'
        if int(value)==1:
            cmd=b'\x01\x00\x00\x00'

        DataArray = client.get_node("ns=1;s=IOLM/Port 5/Attached Device/PDO Data Byte Array")

        DataArray.set_value(cmd)
        time.sleep(1)
        result="Succesful"
    except:
        result="Comunication error"
    return result
    '''try:
        varWrite = b'\x00'
        if int(value)==80:
            varWrite = b'\x80'
        #print("multiply method call with parameters: ", index, subindex)
        parent = client.get_node("ns=1;s=IOLM/Port 5")
        #parent = client.get_node(vnode)
        method = parent.get_child("1:IOLM/Port 5/ISDU Write")
        #method = parent.get_child(vmethod)
        inputs = method.get_child("0:InputArguments").get_value()
        #inputs = method.get_child(vinputs).get_value()
        arguments = [ua.Variant(int(index),ua.VariantType.UInt16),ua.Variant(int(subindex),ua.VariantType.Byte),ua.Variant(varWrite,ua.VariantType.ByteString)]
        result="Error in the function"
        parent.call_method(method, *arguments)
        result="Succesful"
    except TypeError:
        result="Succesful"
    except Exception as i:
        print(type(i))
        result="Error in the function"
    return result'''

@uamethod
def writeRFID(parent,value):
    try:
        ds=b'\x01\x00\x00\x00'
        rd=b'\x01\x00\x00\x00'
        wr=b'\x02\x00\x00\x00'
        dsData=ds+value.encode()
        wrData=wr+value.encode()
        rdData=rd+value.encode()

        DataArray = client.get_node("ns=1;s=IOLM/Port 5/Attached Device/PDO Data Byte Array")
        #DataArray.set_value(dsData)
        #time.sleep(1)
        DataArray.set_value(rdData)
        time.sleep(1)
        DataArray.set_value(wrData)
        time.sleep(1)
        DataArray.set_value(rdData)
        result="Succesful"
    except:
        result="Comunication error"
    return result

#Server Config
server = Server()
server.set_endpoint(urlServer)
name = "OPCUA_SIMULATOR_SERVER"
addspace = server.register_namespace(name)
node = server.get_objects_node()
Param = node.add_object(addspace,"Parameters")
Methods = node.add_object(addspace,"Methods")
VPort1 = Param.add_variable(addspace,"VPort1",0)
VPort2 = Param.add_variable(addspace,"VPort2",0)
VPort3 = Param.add_variable(addspace,"VPort3",0)
VPort4 = Param.add_variable(addspace,"VPort4",0)
VPort5 = Param.add_variable(addspace,"VPort5",0)
VPort6 = Param.add_variable(addspace,"VPort6",0)
VPort7 = Param.add_variable(addspace,"VPort7",0)
VPort8 = Param.add_variable(addspace,"VPort8",0)
VMeasurePort1= Param.add_variable(addspace,"VMeasurePort1",0)
VMeasurePort2= Param.add_variable(addspace,"VMeasurePort2",0)
VMeasurePort3= Param.add_variable(addspace,"VMeasurePort3",0)
VMeasurePort4= Param.add_variable(addspace,"VMeasurePort4",0)
VMeasurePort5= Param.add_variable(addspace,"VMeasurePort5",0)
VMeasurePort6= Param.add_variable(addspace,"VMeasurePort6",0)
VMeasurePort7= Param.add_variable(addspace,"VMeasurePort7",0)
VMeasurePort8= Param.add_variable(addspace,"VMeasurePort8",0)
VPort1PDOArray = Param.add_variable(addspace,"VPort1PDOArray",0)
VPort2PDOArray = Param.add_variable(addspace,"VPort2PDOArray",0)
VPort3PDOArray = Param.add_variable(addspace,"VPort3PDOArray",0)
VPort4PDOArray = Param.add_variable(addspace,"VPort4PDOArray",0)
VPort5PDOArray = Param.add_variable(addspace,"VPort5PDOArray",0)
VPort6PDOArray = Param.add_variable(addspace,"VPort6PDOArray",0)
VPort7PDOArray = Param.add_variable(addspace,"VPort7PDOArray",0)
VPort8PDOArray = Param.add_variable(addspace,"VPort8PDOArray",0)
VTaskActive = Param.add_variable(addspace,"VTaskActive",0)
VMeasurePort1.set_writable()
VMeasurePort2.set_writable()
VMeasurePort3.set_writable()
VMeasurePort4.set_writable()
VMeasurePort5.set_writable()
VMeasurePort6.set_writable()
VMeasurePort7.set_writable()
VMeasurePort8.set_writable()
VPort1.set_writable()
VPort2.set_writable()
VPort3.set_writable()
VPort4.set_writable()
VPort5.set_writable()
VPort6.set_writable()
VPort7.set_writable()
VPort8.set_writable()
VPort1PDOArray.set_writable()
VPort2PDOArray.set_writable()
VPort3PDOArray.set_writable()
VPort4PDOArray.set_writable()
VPort5PDOArray.set_writable()
VPort6PDOArray.set_writable()
VPort7PDOArray.set_writable()
VPort8PDOArray.set_writable()
VTaskActive.set_writable()
#define methods
inargx = ua.Argument()
inargx.Name = "index"
inargx.DataType = ua.NodeId(ua.ObjectIds.Int64)
inargx.ValueRank = -1
inargx.ArrayDimensions = []
inargx.Description = ua.LocalizedText("First number index")
inargy = ua.Argument()
inargy.Name = "subindex"
inargy.DataType = ua.NodeId(ua.ObjectIds.Int64)
inargy.ValueRank = -1
inargy.ArrayDimensions = []
inargy.Description = ua.LocalizedText("Second number subindex")
inargz = ua.Argument()
inargz.Name = "value"
inargz.DataType = ua.NodeId(ua.ObjectIds.Int64)
inargz.ValueRank = -1
inargz.ArrayDimensions = []
inargz.Description = ua.LocalizedText("Value")
outarg = ua.Argument()
outarg.Name = "Result"
outarg.DataType = ua.NodeId(ua.ObjectIds.Int64)
outarg.ValueRank = -1
outarg.ArrayDimensions = []
outarg.Description = ua.LocalizedText("ISDU Write result")
multiply_node = Methods.add_method(addspace, "ISDU Write", ISDUWrite, [inargx, inargy,inargz], [outarg])

#define methods
inargx = ua.Argument()
inargx.Name = "index"
inargx.DataType = ua.NodeId(ua.ObjectIds.String)
inargx.ValueRank = -1
inargx.ArrayDimensions = []
inargx.Description = ua.LocalizedText("Message")
outarg = ua.Argument()
outarg.Name = "Result"
outarg.DataType = ua.NodeId(ua.ObjectIds.Int64)
outarg.ValueRank = -1
outarg.ArrayDimensions = []
outarg.Description = ua.LocalizedText("ISDU Write result")
rftag_node = Methods.add_method(addspace, "writeRFID", writeRFID, [inargx], [outarg])

server.start()
print("Server starter at {}".format(urlServer))


print("Client connected")

valPort1=client.get_node("ns=1;s=IOLM/Port 1/Attached Device/PDI Data Unsigned32")
valPort2=client.get_node("ns=1;s=IOLM/Port 2/Attached Device/PDI Data Unsigned32")
valPort3=client.get_node("ns=1;s=IOLM/Port 3/Attached Device/PDI Data Unsigned32")
valPort4=client.get_node("ns=1;s=IOLM/Port 4/Attached Device/PDI Data Unsigned32")
valPort5=client.get_node("ns=1;s=IOLM/Port 5/Attached Device/PDI Data Byte String")
valPort6=client.get_node("ns=1;s=IOLM/Port 6/Attached Device/PDI Data Byte String")
valPort7=client.get_node("ns=1;s=IOLM/Port 7/Attached Device/PDI Data Byte String")
valPort8=client.get_node("ns=1;s=IOLM/Port 8/Attached Device/PDI Data Byte String")

try:
    valPort3PDIMeasure=client.get_node("ns=1;s=IOLM/Port 3/Attached Device/PDI Fields/1.2097667.PD_ProcessData0.TN_PDin_AD12.4.12",)
except:
    pass


valPort1PDOArray=client.get_node("ns=1;s=IOLM/Port 1/Attached Device/PDO Data Byte Array")
valPort2PDOArray=client.get_node("ns=1;s=IOLM/Port 2/Attached Device/PDO Data Byte Array")
valPort3PDOArray=client.get_node("ns=1;s=IOLM/Port 3/Attached Device/PDO Data Byte Array")
valPort4PDOArray=client.get_node("ns=1;s=IOLM/Port 4/Attached Device/PDO Data Byte Array")
valPort5PDOArray=client.get_node("ns=1;s=IOLM/Port 5/Attached Device/PDO Data Byte Array")
valPort6PDOArray=client.get_node("ns=1;s=IOLM/Port 6/Attached Device/PDO Data Byte Array")
valPort7PDOArray=client.get_node("ns=1;s=IOLM/Port 7/Attached Device/PDO Data Byte Array")
valPort8PDOArray=client.get_node("ns=1;s=IOLM/Port 8/Attached Device/PDO Data Byte Array")      
valTaskActive=client.get_node("ns=1;s=IOLM/Port 5/Attached Device/PDI Fields/PD_ProcessDataEasy/1.4194561.PD_ProcessDataEasy.TN_dtRun.250.1")
LastVPort1PDOArray=None;
LastVPort2PDOArray=None;
LastVPort3PDOArray=None;
LastVPort4PDOArray=None;
LastVPort5PDOArray=None;
LastVPort6PDOArray=None;
LastVPort7PDOArray=None;
LastVPort8PDOArray=None;
testConnection=True
while True:
    try:
        #Read IOLink variables

        try:
            VMeasurePort3.set_value(valPort3PDIMeasure.get_value())
        except:
            pass
        
        try:
            VPort1.set_value(valPort1.get_value())
        except:
            pass
        try:
            VPort2.set_value(valPort2.get_value())
        except:
            pass
        try:
            VPort3.set_value(valPort3.get_value())
        except:
            pass
        try:
            VPort4.set_value(valPort4.get_value())
        except:
            pass
        try:
            VPort5.set_value(valPort5.get_value())
        except:
            pass
        try:
            VPort6.set_value(valPort6.get_value())
        except:
            pass
        try:
            VPort7.set_value(valPort7.get_value())
        except:
            pass
        try:
            VPort8.set_value(valPort8.get_value())
        except:
            pass
        try:
            if LastVPort1PDOArray!=VPort1PDOArray.get_value() and LastVPort1PDOArray != None:
                arr = bytes(VPort1PDOArray.get_value())
                valPort1PDOArray.set_value(arr)
            else:
                VPort1PDOArray.set_value(valPort1PDOArray.get_value())
            LastVPort1PDOArray=VPort1PDOArray.get_value();
        except Exception as e:
            pass
            #print(e)
        try:
            if LastVPort2PDOArray!=VPort1PDOArray.get_value() and LastVPort2PDOArray != None:
                arr = bytes(VPort2PDOArray.get_value())
                valPort2PDOArray.set_value(arr)
            else:
                VPort2PDOArray.set_value(valPort2PDOArray.get_value())
            LastVPort2PDOArray=VPort2PDOArray.get_value();
        except Exception as e:
            pass
            #print(e)
        try:
            if LastVPort3PDOArray!=VPort3PDOArray.get_value() and LastVPort3PDOArray != None:
                arr = bytes(VPort3PDOArray.get_value())
                valPort3PDOArray.set_value(arr)
            else:
                VPort3PDOArray.set_value(valPort3PDOArray.get_value())
            LastVPort3PDOArray=VPort3PDOArray.get_value();
        except Exception as e:
            pass
            #print(e)
        try:
            if LastVPort4PDOArray!=VPort4PDOArray.get_value() and LastVPort4PDOArray != None:
                arr = bytes(VPort4PDOArray.get_value())
                valPort4PDOArray.set_value(arr)
            else:
                VPort4PDOArray.set_value(valPort4PDOArray.get_value())
            LastVPort4PDOArray=VPort4PDOArray.get_value();
        except Exception as e:
            pass
            #print(e)            
        try:
            if LastVPort5PDOArray!=VPort5PDOArray.get_value() and LastVPort5PDOArray != None:
                arr = bytes(VPort5PDOArray.get_value())
                valPort5PDOArray.set_value(arr)
            else:
                VPort5PDOArray.set_value(valPort5PDOArray.get_value())
            LastVPort5PDOArray=VPort5PDOArray.get_value();
        except Exception as e:
            pass
            #print(e)
        try:
            if LastVPort6PDOArray!=VPort6PDOArray.get_value() and LastVPort6PDOArray != None:
                arr = bytes(VPort6PDOArray.get_value())
                valPort6PDOArray.set_value(arr)
            else:
                VPort6PDOArray.set_value(valPort6PDOArray.get_value())
            LastVPort6PDOArray=VPort6PDOArray.get_value();
        except Exception as e:
            pass
            #print(e)
        try:
            if LastVPort8PDOArray!=VPort8PDOArray.get_value() and LastVPort8PDOArray != None:
                arr = bytes(VPort8PDOArray.get_value())
                valPort8PDOArray.set_value(arr)
            else:
                VPort8PDOArray.set_value(valPort8PDOArray.get_value())
            LastVPort8PDOArray=VPort8PDOArray.get_value();
        except Exception as e:
            pass
            #print(e)
        try:
            if LastVPort7PDOArray!=VPort7PDOArray.get_value() and LastVPort7PDOArray != None:
                arr = bytes(VPort7PDOArray.get_value())
                valPort7PDOArray.set_value(arr)
            else:
                VPort7PDOArray.set_value(valPort7PDOArray.get_value())
            LastVPort7PDOArray=VPort7PDOArray.get_value();
        except Exception as e:
            pass
            #print(e)
        try:
            VTaskActive.set_value(valTaskActive.get_value())
        except:
            pass
        time.sleep(0.5)
        if testConnection:
            print("Connection Ok")
            testConnection=False
    except ConnectionAbortedError:
        print("CE ping to module is:")
        time.sleep(10)
        r = ping(ipModulo)
        print(r)
        if r != None:
            print("Success")
            client.connect()
            testConnection=True
    except BrokenPipeError:
        print("BK ping to module is:")
        time.sleep(10)
        r = ping(ipModulo)
        print(r)
        if r != None:
            print("Success")
            client.connect()
            testConnection=True
    except TimeoutError:
        print("TO ping to module is:")
        time.sleep(10)
        r = ping(ipModulo)
        print(r)
        if r != None:
            print("Success")
            client.connect()
            testConnection=True
    except Exception as e:
        if "concurrent.futures._base.TimeoutError" in str(e):
            r = ping(ipModulo)
            print(r)
            if r != None:
                print("Success")
                client.connect()
        now=datetime.now()
        dt_string = now.strftime("%d/%m/%Y %H:%M:%S")
        print("No connection to module at:",dt_string)
        print(type(e))
        print(e)
        time.sleep(10)
        testConnection=True
    
client.disconnect()

